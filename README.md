Test-Appsolute:

les technologies utilisées sont Symfony4, HTML/CSS, Twig, Docker, Bootstrap et MySql.

Les fonctionnalités : 

-les utilisateurs peuvent s'inscrire.
-Ils peuvent se "login/logout"
-L'utilisateur peut voir les messages postés s'il est connecté.
-Il peut ajouter un message dans le fil d'actualité
-Il peut aller voir les messages d'un utilisateur spécifique.
-Il peut aller dans son menu de gestion pour supprimer/"update" ses messages.
-Il peut aussi "like/dislike" un post.

Les messages s'affichent dans l'ordre des postes du plus récent au plus vieux, si on l'update le message repassera en premier dans le flux.
Si on "like" un message on ne peut pas le "like"encore une fois pareil pour le "dislike".
Le dernier utilisateur qui a posté un message s'affiche en haut de la page ainsi que le titre de son poste
Un utilisateur "login" ne peut pas revenir sur la page de "login".