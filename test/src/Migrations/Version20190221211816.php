<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221211816 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE message CHANGE user_id user_id INT DEFAULT NULL, CHANGE like_message like_message INT DEFAULT NULL, CHANGE dislike_message dislike_message INT DEFAULT NULL, CHANGE text contents LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE is_liked CHANGE user_relation_id user_relation_id INT DEFAULT NULL, CHANGE message_relation_id message_relation_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE is_liked CHANGE user_relation_id user_relation_id INT DEFAULT NULL, CHANGE message_relation_id message_relation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE message CHANGE user_id user_id INT DEFAULT NULL, CHANGE like_message like_message INT DEFAULT NULL, CHANGE dislike_message dislike_message INT DEFAULT NULL, CHANGE contents text LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
