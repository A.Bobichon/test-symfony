<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\StringType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{
    /**
     * @Route("/logout", name="logout")
     *
     */
    public function logout()
    {
        //method for logout path set in services.yaml
        return $this->render('/');
    }

    /**
     * @Route("/user", name="add_user")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // Set the form for a user
        $user = new User();
        $form = $this->createFormBuilder($user)
            ->add("username", EmailType::class)
            ->add("name", TextType::class)
            ->add("lastname", TextType::class)
            ->add("password", PasswordType::class)
            ->add("repeatPassword", PasswordType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        // Post the data user
        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            if ($user->getPassword() === $user->getRepeatPassword()) {

                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute("app_login");
            }
        }

        return $this->render('user/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
