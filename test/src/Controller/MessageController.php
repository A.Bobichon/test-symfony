<?php

namespace App\Controller;

use App\Service\LikeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Message;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Repository\UserRepository;
use App\Repository\MessageRepository;
use App\Entity\User;

class MessageController extends AbstractController
{
    /**
     * @Route("/security/message/add", name="add_messages")
     * @Route("/security/message/modif/{id}", name="modif_message")
     */
    public function addMessage(Request $request, Message $message = null)
    {
        $user = $this->getUser();
        // Check if we take a message for the update
        if (!$message) {
            $message = new Message();
        }
        // Set the form
        $form = $this->createFormBuilder($message)
            ->add('title', TextType::class)
            ->add('contents', TextareaType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        // Post the message in the database or update the message
        if ($form->isSubmitted() && $form->isValid() && $this->getUser() !== null) {
            $message->setUser($user);

            $date = new \DateTime();
            $message->setDate($date);

            $message = $form->getData();
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($message);
            $manager->flush();

            $messages = $this->getDoctrine()->getRepository(Message::class)->findBy(array("user" => $user));

            $this->render("message/menu.html.twig", ["messages" => $messages]);
            return $this->redirectToRoute("all_messages");
        }

        // Create the form view
        return $this->render('message/addForm.html.twig', [
            "form" => $form->createView(),
            "user" => $user,
        ]);
    }

    /**
     * @Route("/", name="all_messages")
     */
    public function foundAllMessage(MessageRepository $repo)
    {
        // Found all the messages of all users
        $getMessages = $this->getDoctrine()->getRepository(Message::class);
        $messages = $getMessages->findAll();

        $lastMessage = end($messages);

        if (empty($lastMessage)){
            $lastMessage = null;
        }

        return $this->render("home.html.twig", [
            "messages" => $messages,
            "lastMessage" => $lastMessage
        ]);
    }

    /**
     * @Route("/security/message/{id}", name="message_user")
     */
    public function foundMessageUser(MessageRepository $messageRepository)
    {
        // Finds the messages of the user connect
        $user = $this->getUser();
        $messages = $messageRepository->findBy(array("user" => $user->getId()));

        return $this->render("message/menu.html.twig", [
                "messages" => $messages,
                "user" => $user
        ]);
    }

    /**
     * @Route("/security/message/targetUser/{id}", name="message_user_target")
     */
    public function foundMessageTargetUser(MessageRepository $messageRepository, UserRepository $userRepository, User $user)
    {
        // Finds the messages of the target user
        $user = $userRepository->find($user->getId());
        $messages = $messageRepository->findBy(array("user" => $user->getId()));

        return $this->render("message/userChoice.html.twig", [
                "messages" => $messages,
                "user" => $user
        ]);
    }

    /**
     * @Route("/security/message/delete/{id}", name="message_delete")
     */
    public function deleteMessage(Message $message, MessageRepository $messageRepository)
    {
        // Deletes the selected message
        $manager = $this->getDoctrine()->getManager();
        $messageRemove = $messageRepository->find($message->getId());
        $manager->remove($messageRemove);
        $manager->flush();

        $user = $this->getUser();
        $messages = $messageRepository->findBy(array("user" => $user));

        return $this->render("message/menu.html.twig", [
            "messages" => $messages
        ]);
    }

    /**
     * @Route("/security/message/like/{id}", name="like_message")
     * @Route("/security/message/userChoice/like/{id}", name="like_message_userChoice")
     * 
     * @Route("/security/message/dislike/{id}", name="dislike_message")
     * @Route("/security/message/userChoice/dislike/{id}", name="dislike_message_userChoice")
     */
    public function LikeOrDislikeAction(MessageRepository $messageRepository, Message $message, Request $request, LikeService $likeService)
    {
        // Performs the action like/dislike of the user (service > LikeService.php)
        $user = $this->getUser();
        $currentRoute = $request->attributes->get('_route');
        $messageLike = $messageRepository->find($message->getId());
        $manager = $this->getDoctrine()->getManager();

        // Use the LikeService
        $messageLikeOrDislike = $likeService->LikeOrDislikeAction($user, $messageLike, $currentRoute);

        // Update the message change with his relation table isLiked
        $manager->persist($messageLikeOrDislike[0]);
        $manager->persist($messageLikeOrDislike[1]);
        $manager->flush();

        $messages = $messageRepository->findAll();
        $lastMessage = end($messages);

        // Render the route for a like message in the home page
        if ($currentRoute === "like_message" || $currentRoute === "dislike_message") {
            return $this->render("home.html.twig", [
                "messages" => $messages,
                "lastMessage" => $lastMessage
            ]);

        // Render the route for a like message in the page user choisen
        } elseif ($currentRoute === "like_message_userChoice" || $currentRoute === "dislike_message_userChoice") {

            $userValid = $this->getDoctrine()->getRepository(Message::class)->find($messageLike)->getUser();

            return $this->render("message/userChoice.html.twig", [
                "messages" => $messages,
                "user" => $userValid
            ]);
        }
    }
}
