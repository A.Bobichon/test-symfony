<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IsLikedRepository")
 */
class IsLiked
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="isLikeds")
     */
    private $userRelation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Message", inversedBy="isLikeds")
     */
    private $messageRelation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $liked;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disliked;

    public function __construct()
    {
        $this->liked = false;
        $this->disliked = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserRelation(): ?User
    {
        return $this->userRelation;
    }

    public function setUserRelation(?User $userRelation): self
    {
        $this->userRelation = $userRelation;

        return $this;
    }

    public function getMessageRelation(): ?Message
    {
        return $this->messageRelation;
    }

    public function setMessageRelation(?Message $messageRelation): self
    {
        $this->messageRelation = $messageRelation;

        return $this;
    }

    public function getLiked(): ?bool
    {
        return $this->liked;
    }

    public function setLiked(bool $liked): self
    {
        $this->liked = $liked;

        return $this;
    }

    public function getDisliked(): ?bool
    {
        return $this->disliked;
    }

    public function setDisliked(bool $disliked): self
    {
        $this->disliked = $disliked;

        return $this;
    }
}
