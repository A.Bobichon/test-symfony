<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $contents;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messageUser")
     */
    private $user;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $likeMessage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dislikeMessage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IsLiked", mappedBy="messageRelation", cascade={"persist", "remove"})
     */
    private $isLikeds;

    public function __construct()
    {
        $this->isLikeds = new ArrayCollection();
        $this->likeMessage = 0;
        $this->dislikeMessage = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContents(): ?string
    {
        return $this->contents;
    }

    public function setContents(string $contents): self
    {
        $this->contents = $contents;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLikeMessage(): ?int
    {
        return $this->likeMessage;
    }

    public function setLikeMessage(int $likeMessage): self
    {
        $this->likeMessage = $likeMessage;

        return $this;
    }

    public function getDislikeMessage(): ?int
    {
        return $this->dislikeMessage;
    }

    public function setDislikeMessage(int $dislikeMessage): self
    {
        $this->dislikeMessage = $dislikeMessage;

        return $this;
    }

    /**
     * @return Collection|IsLiked[]
     */
    public function getIsLikeds(): Collection
    {
        return $this->isLikeds;
    }

    public function addIsLiked(IsLiked $isLiked): self
    {
        if (!$this->isLikeds->contains($isLiked)) {
            $this->isLikeds[] = $isLiked;
            $isLiked->setMessageRelation($this);
        }

        return $this;
    }

    public function removeIsLiked(IsLiked $isLiked): self
    {
        if ($this->isLikeds->contains($isLiked)) {
            $this->isLikeds->removeElement($isLiked);
            // set the owning side to null (unless already changed)
            if ($isLiked->getMessageRelation() === $this) {
                $isLiked->setMessageRelation(null);
            }
        }

        return $this;
    }
}
