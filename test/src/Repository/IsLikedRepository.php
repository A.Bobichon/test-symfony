<?php

namespace App\Repository;

use App\Entity\IsLiked;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IsLiked|null find($id, $lockMode = null, $lockVersion = null)
 * @method IsLiked|null findOneBy(array $criteria, array $orderBy = null)
 * @method IsLiked[]    findAll()
 * @method IsLiked[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IsLikedRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IsLiked::class);
    }

    // /**
    //  * @return IsLiked[] Returns an array of IsLiked objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IsLiked
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
