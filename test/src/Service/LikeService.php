<?php

namespace App\Service;

use App\Entity\IsLiked;
use App\Entity\Message;
use App\Entity\User;


class LikeService
{
    // Method to perform the like/dislike action
    public function LikeOrDislikeAction(User $user, Message $messageLike, $currentRoute)
    {
        $isLiked = new IsLiked();
        // Finds the relationship table between messages and the user
        for ($i = 0; $i < count($user->getIsLikeds()); $i++) {
            if ($user->getIsLikeds()[$i]->getMessageRelation()->getId() === $messageLike->getId()) {
                $isLiked = $user->getIsLikeds()[$i];
            }
        }
        // Finds the value of likes/dislikes
        $valueLike = $messageLike->getLikeMessage();
        $valueDisLike = $messageLike->getDislikeMessage();

        // Method of adding like
        if ($currentRoute === "like_message" || $currentRoute === "like_message_userChoice") {
            if ($isLiked->getUserRelation() === null && $isLiked->getMessageRelation() === null) {
                $isLiked->setUserRelation($user);
                $isLiked->setMessageRelation($messageLike);
            }
            if ($isLiked->getLiked() === false) {
                $finalResult = $valueLike + 1;
                $messageLike->setLikeMessage($finalResult);
                $isLiked->setLiked(true);
            } else {
                $finalResult = $valueLike - 1;
                $messageLike->setLikeMessage($finalResult);
                $isLiked->setLiked(false);
            }

        // Method of adding dislike
        } elseif ($currentRoute === "dislike_message" || $currentRoute === "dislike_message_userChoice") {
            if ($isLiked->getUserRelation() === null && $isLiked->getMessageRelation() === null) {
                $isLiked->setUserRelation($user);
                $isLiked->setMessageRelation($messageLike);
            }
            if ($isLiked->getDisliked() === false) {
                $finalResult = $valueDisLike + 1;
                $messageLike->setDislikeMessage($finalResult);
                $isLiked->setDisliked(true);
            } else {
                $finalResult = $valueDisLike - 1;
                $messageLike->setDislikeMessage($finalResult);
                $isLiked->setDisliked(false);
            }
        }

        // Return the message and the table isLiked with the new value
        return array($isLiked, $messageLike);
    }

}